<?php

namespace App\Library\Common;

Trait Connection 
{
	/*
	 * get data via Curl
	 * method GET
	 * inputs: url, header
	 * outputs: array 
	 *
	 */
	public function useCURL($url='',$header=[], $method='get')
	{
		# code...
	    $options = array(
	            CURLOPT_RETURNTRANSFER => 1, 
	            CURLOPT_USERAGENT      => "Mozilla/5.0",         
	    );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt_array($curl, $options);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
	}
}