<?php

namespace App\Library\Common;

use Response;

Trait Export 
{

	/*
	 * will create CSVFile from Array
	 * inputs: array
	 * outputs: .csv file 
	 *
	 */
	public function createCSV($arrdata=[])
	{

		$filename = "data_export_" . date("Y-m-d") . ".csv";
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download  
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename=".$filename);
		header("Content-Transfer-Encoding: binary");

		$df = fopen("php://output", 'w');
		foreach ($arrdata as $row) {
		    fputcsv($df, $row);
		}
		fclose($df);
	}

	/*
	 * will create JsonFile from Array
	 * inputs: array
	 * outputs: .json file 
	 *
	 */
	public function createJSON($jsondata)
	{
		$filename = "data_export_" . date("Y-m-d") . ".json";
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download  
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header('Content-disposition: attachment; filename='.$filename);
		header('Content-type: application/json');

		$fp = fopen('php://output', 'w');
	    fwrite($fp, $jsondata);
		fclose($fp);
		// file_put_contents($filename, $jsondata);
	}

	#will create xmlFile from Array
	public function createXML($arrdata=[])
	{
		# Code...
	}
} 