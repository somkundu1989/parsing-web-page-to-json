<?php

namespace App\Library;

use Illuminate\Http\Request;
use App\Library\Common\Connection;
use App\Library\Common\Export;

class Weather 
{
	use Connection, Export;

    private $apiId='2eaf7d0e9bf54a21a98e51bd897e6b77';
    private $baseUrl='https://api.openweathermap.org/data/2.5/weather';
    private $resultData;
    # https://api.openweathermap.org/data/2.5/weather?q=Kolkata,in&appid=2eaf7d0e9bf54a21a98e51bd897e6b77

    public function __construct($query='Kolkata,in')
    {
    	# code...
    	$url=$this->baseUrl.'?q='.$query.'&appid='.$this->apiId;
        $this->resultData = $this->useCURL($url);
        // dd($this->resultData);
        
        return $this;        
    }

    public function getRestlt()
    {
        $this->createJSON(json_encode(json_decode($this->resultData),JSON_PRETTY_PRINT)); 
    }
}
