<?php

namespace App\Library;

use Illuminate\Http\Request;
use App\Library\Common\Connection;
use App\Library\Common\Export;
use KubAT\PhpSimple\HtmlDomParser;

class HtmlToJsonParser 
{
	use Connection, Export;

    public $bodyData;
    public $resultArr;

    public function __construct($url='http://www.mycorporateinfo.com/business/kamdhenu-engineering-industries-ltd')
    {
    	# code...
        $this->bodyData = $this->useCURL($url);
        $this->resultArr = [];
        
        return $this;        
    }

    public function parse()
    {
    	$htmlcontent = $this->bodyData;

		$thedom = new \DOMDocument();
		@$thedom->loadHTML($htmlcontent);

        $tables = $thedom->getElementsByTagName('table');
		// dd($tables->parentNode());
        foreach ($tables as $index=>$tableElement) {
            // echo $tableElement.'<br/>'; 
            $arrayTitle='';
            if($tableElement->parentNode->getAttribute('id')!=''){
                $arrayTitle=$tableElement->parentNode->getAttribute('id');
            }
            if($tableElement->parentNode->parentNode->getAttribute('id')!=''){
                $arrayTitle=$tableElement->parentNode->parentNode->getAttribute('id');
            }
            if($tableElement->parentNode->parentNode->parentNode->getAttribute('id')!=''){
                $arrayTitle=$tableElement->parentNode->parentNode->parentNode->getAttribute('id');
            }
            if($tableElement->parentNode->parentNode->parentNode->parentNode->getAttribute('id')!=''){
                $arrayTitle=$tableElement->parentNode->parentNode->parentNode->parentNode->getAttribute('id');
            }
                
            
            // echo $arrayTitle.'.............<br/>';
            $tempTableArr=[];
            # code...
            $header = $tableElement->getElementsByTagName('th');
            $tabledata = $tableElement->getElementsByTagName('td');

            if($header->count()>0){
        		$headerArr=[];
        		// dd([$header, $tabledata]);
        	    //#Get header name of the table
        		foreach($header as $val){
        			$headerArr[] = trim($val->textContent);
        		}
        		// dd($headerArr); die();

        		//#Get row data/detail table without header name as key
        		$tempArr=[];
        		foreach($tabledata as $key=>$val){
        			// echo $key. '-'.$headerArr[ ( $key % count($headerArr) ) ] .'='. trim($val->textContent).'<br>';
        			$tempArr[$headerArr[ ( $key % count($headerArr) ) ]] = trim($val->textContent);
        			if((($key+1) >= count($headerArr)) && (($key+1) % count($headerArr) == 0)){
                        // print_r($tempArr);
        				$tempTableArr[]=$tempArr;
        				$tempArr=[];
        			}
        		}
            }else{
                $tempArr=[];
                foreach($tabledata as $key=>$val){ 
                    if(($key+1) %2 == 0){
                        // echo trim($tabledata[ $key-1 ]->textContent). '=>' . trim($val->textContent);
                        $tempArr[trim($tabledata[ $key-1 ]->textContent)] = trim($val->textContent);
                    }
                }
                $tempTableArr=$tempArr;
            }
                
            // echo '<pre>';
            // print_r($tempTableArr);echo '<br/>';
            $this->resultArr[$arrayTitle]=$tempTableArr;
        }

    	return $this;
    }
    public function expertToJSON()
    {
    	// dd($this->resultArr);
    	# code...
        $this->createJSON(json_encode($this->resultArr,JSON_PRETTY_PRINT)); 
    }
    public function expertToCSV()
    {
        /*
         * Common function convert array to printable array
         * inputs: array
         * outputs: array 
         *
         */
        $csvHeader = []; 
        $csvHeader = array_keys($this->resultArr);
        $csvBody = [];
        foreach ($this->resultArr as $row)
        {
            $temp=[];
            foreach($row as $key=>$element){
                // dd([[$key=>$element], array_keys($row)]);
                if(!in_array($key, $csvHeader)){
                    $csvHeader[] = $key;
                }

                if(is_array($element)){
                    $temp[array_search($key,$csvHeader)]=json_encode($element);
                }else{
                    $temp[array_search($key,$csvHeader)]=$element;
                }
            }
            $csvBody[] = $temp; 
        }
        $csvData[] = $csvHeader;
        $this->createCSV(array_merge($csvData, $csvBody)); 
    }
}
